﻿(function($) {
  Drupal.behaviors.slider_filter = {
    attach: function(context, settings) {
      $('.slider-widget-wrapper').each(function() {
        var $sliderWrapper = $(this);
        $('<div class="slider-widget" />').appendTo($sliderWrapper).slider({
          range: true,
          min: 50, // Adjust slider min and max to the range
          max: 150,    // of the exposed filter.
          values: [$sliderWrapper.find('input:first').val(), $sliderWrapper.find('input:last').val()],
          slide: function(event, ui) {
            $sliderWrapper.find('input:first').val(ui.values[0]);
            $sliderWrapper.find('input:last').val(ui.values[1]);
          },
	  stop: function(event, ui){
            $(this).parents('form').find('.ctools-auto-submit-click').click();
          }
        });
      });
    
	$('.slider-age-widget-wrapper').each(function() {
	var $sliderWrapper = $(this);
        $('<div class="slider-widget" />').appendTo($sliderWrapper).slider({
          range: true,
          min: 15, // Adjust slider min and max to the range
          max: 40,    // of the exposed filter.
          values: [$sliderWrapper.find('input:first').val(), $sliderWrapper.find('input:last').val()],
          slide: function(event, ui) {
            $sliderWrapper.find('input:first').val(ui.values[0]);
            $sliderWrapper.find('input:last').val(ui.values[1]);
          },
	  stop: function(event, ui){
            $(this).parents('form').find('.ctools-auto-submit-click').click();
          }
        });
      });
    
	$('.slider-growth-widget-wrapper').each(function() {
	var $sliderWrapper = $(this);
        $('<div class="slider-widget" />').appendTo($sliderWrapper).slider({
          range: true,
          min: 160, // Adjust slider min and max to the range
          max: 220,    // of the exposed filter.
          values: [$sliderWrapper.find('input:first').val(), $sliderWrapper.find('input:last').val()],
          slide: function(event, ui) {
            $sliderWrapper.find('input:first').val(ui.values[0]);
            $sliderWrapper.find('input:last').val(ui.values[1]);
          },
	  stop: function(event, ui){
            $(this).parents('form').find('.ctools-auto-submit-click').click();
          }
        });
      });
    }
  };
})(jQuery);


